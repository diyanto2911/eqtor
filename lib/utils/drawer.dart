import 'package:eqtor/utils/screen_config.dart';
import 'package:flutter/material.dart';

Widget drawer({BuildContext context}){
  return Drawer(
    elevation: 3,
    child: Container(
      color: Colors.blue,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Center(
              child: Text(
                "PROFILE",
                style: TextStyle(
                    fontSize: ScreenConfig.blockHorizontal * 6,
                    color: Colors.white),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Icon(
              Icons.account_circle,
              size: ScreenConfig.blockHorizontal * 25,
              color: Colors.white,
            ),
            Center(child: Text("My Account",style: TextStyle(color: Colors.white),textScaleFactor: 1.2,)),
            SizedBox(height: 30,),
            ListTile(
              leading: Icon(Icons.account_circle,size: 30,color: Colors.white,),
              title: Text("My Account",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

            ),
            Divider(color: Colors.white,),
            ListTile(
              leading: Icon(Icons.chrome_reader_mode,size: 30,color: Colors.white,),
              title: Text("News",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

            ),
            Divider(color: Colors.white,),
            ListTile(

              leading: Icon(Icons.info,size: 30,color: Colors.white,),
              title: Text("Inforamation",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

            ),
            Divider(color: Colors.white,),
            ListTile(
              leading: Icon(Icons.settings,size: 30,color: Colors.white,),
              title: Text("Setting",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

            ),
            Divider(color: Colors.white,),
            ListTile(
              leading: Icon(Icons.account_circle,size: 30,color: Colors.white,),
              title: Text("LogOut",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

            ),
            Divider(color: Colors.white,),
          ],
        ),
      ),
    ),
  );
}
