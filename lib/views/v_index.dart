import 'package:eqtor/utils/drawer.dart';
import 'package:eqtor/utils/screen_config.dart';
import 'package:eqtor/views/v_account.dart';
import 'package:eqtor/views/v_graffic.dart';
import 'package:eqtor/views/v_home.dart';
import 'package:eqtor/views/v_information.dart';
import 'package:eqtor/views/v_login.dart';
import 'package:eqtor/views/v_news.dart';
import 'package:flutter/material.dart';
import 'package:bmnav/bmnav.dart' as bmnav;

class ViewIndex extends StatefulWidget {
  @override
  _ViewIndexState createState() => _ViewIndexState();
}

class _ViewIndexState extends State<ViewIndex> {
  int currentTab = 0;
  GlobalKey<ScaffoldState> _globalKey=GlobalKey<ScaffoldState>();
  var title="Home";
  final List<Widget> screens = [
    ViewHome(),
    ViewInformation(),
    ViewNews(),
    ViewGrahp(),
  ];
  Widget currentScreen = ViewHome();

  final PageStorageBucket bucket = PageStorageBucket();
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(title,style: TextStyle(color: Colors.white),),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewAccount())),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.account_circle,
                size: ScreenConfig.blockHorizontal * 10,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      drawer: Drawer(
        elevation: 3,
        child: Container(
          color: Colors.blue,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: [
                Center(
                  child: Text(
                    "PROFILE",
                    style: TextStyle(
                        fontSize: ScreenConfig.blockHorizontal * 6,
                        color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Icon(
                  Icons.account_circle,
                  size: ScreenConfig.blockHorizontal * 25,
                  color: Colors.white,
                ),
                Center(child: Text("My Account",style: TextStyle(color: Colors.white),textScaleFactor: 1.2,)),
                SizedBox(height: 30,),
                ListTile(
                  onTap: (){
                    setState(() {
                      title="Home";
                      currentTab=0;
                      currentScreen = screens[0];
                      Navigator.pop(context);
                    });
                  },
                  leading: Icon(Icons.home,size: 30,color: Colors.white,),
                  title: Text("Home",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

                ),

                Divider(color: Colors.white,),
                ListTile(
                  onTap: (){
                    setState(() {
                      title="Information";
                      currentTab=1;
                      currentScreen = screens[1];
                      Navigator.pop(context);
                    });
                  },
                  leading: Icon(Icons.info,size: 30,color: Colors.white,),
                  title: Text("Inforamation",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

                ),
                Divider(color: Colors.white,),
                ListTile(
                  onTap: (){
                    setState(() {
                      title="News";
                      currentTab=2;
                      currentScreen = screens[2];
                      Navigator.pop(context);
                    });
                  },
                  leading: Icon(Icons.chrome_reader_mode,size: 30,color: Colors.white,),
                  title: Text("News",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

                ),
                Divider(color: Colors.white,),

                ListTile(
                  onTap: (){
                    setState(() {
                      title="Informasi";
                      currentTab=3;
                      currentScreen = screens[3];
                      Navigator.pop(context);
                    });
                  },
                  leading: Icon(Icons.score,size: 30,color: Colors.white,),
                  title: Text("Graphic",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

                ),
                Divider(color: Colors.white,),
                ListTile(
                  onTap: ()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> ViewLogin())),
                  leading: Icon(Icons.account_circle,size: 30,color: Colors.white,),
                  title: Text("LogOut",style: TextStyle(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*4),),

                ),
                Divider(color: Colors.white,),
              ],
            ),
          ),
        ),
      ),
//      drawer: drawer(context: context),
      body: PageStorage(child: currentScreen, bucket: bucket),
      bottomNavigationBar: bmnav.BottomNav(
        index: currentTab,
        iconStyle: bmnav.IconStyle(),
        labelStyle: bmnav.LabelStyle(visible: false),
        onTap: (i) {
          setState(() {
            currentTab = i;
            print(i);

            currentScreen = screens[i];
            if(i==0){
              title="Home";
            }else if(i==1){
              title="Informasi";
            }else if(i==2){
              title="News";
            }else if(i==3){
              title="Graphic";
            }
          });
        },
        items: [
          bmnav.BottomNavItem(Icons.home,),
          bmnav.BottomNavItem(Icons.info),
          bmnav.BottomNavItem(Icons.chrome_reader_mode),
          bmnav.BottomNavItem(Icons.score)
        ],
      ),
    );
  }
}
