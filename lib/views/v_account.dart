import 'package:eqtor/utils/screen_config.dart';
import 'package:flutter/material.dart';

class ViewAccount extends StatefulWidget {
  @override
  _ViewAccountState createState() => _ViewAccountState();
}

class _ViewAccountState extends State<ViewAccount> {
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Align(
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.account_circle,
                    size: ScreenConfig.blockHorizontal * 30,
                    color: Colors.blue,
                  ),
                ),

                Align(
                    alignment: Alignment.center,
                    child: Text(
                      "My Account",
                      textScaleFactor: 1.4,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blue),
                    )),

                SizedBox(height: 30,),

              ],

            ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              color: Colors.grey[200],
              child: Text("General",style: TextStyle(letterSpacing: 2,fontWeight: FontWeight.bold,fontSize: 14),),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Fullname",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("Rizky",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Last Name",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("Mustofa",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Second Name",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("Rizky",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Email",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("Rizky@gmail.com",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Mobile",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("09876555",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
            SizedBox(height: 30,),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              color: Colors.grey[200],
              child: Text("More",style: TextStyle(letterSpacing: 2,fontWeight: FontWeight.bold,fontSize: 14),),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Phone",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("+6239343493",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0,left: 10,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Date of birth",style: TextStyle(fontSize: 16),),
//                  SizedBox(width: ScreenConfig.blockHorizontal*10,),
                  Text("05/04/1998",style: TextStyle(fontSize: 16),),
                ],
              ),
            ),
            Divider(thickness: 1,),
          ],
        ),
      ),
    );
  }
}
