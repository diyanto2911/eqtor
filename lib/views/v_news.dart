import 'package:eqtor/utils/screen_config.dart';
import 'package:flutter/material.dart';

class ViewNews extends StatefulWidget {
  @override
  _ViewNewsState createState() => _ViewNewsState();
}

class _ViewNewsState extends State<ViewNews> {
  List<ListNews> list = [];

  Future<void> addListNews() {
    setState(() {
      list.add(ListNews(
          title: "Lombok Telah Diguncang Gempa Bumi sangat hebat",
          image: "assets/gempa_bumi.jpg"));
      list.add(ListNews(
          title: "Padang  Telah Diguncang Gempa Bumi  11 Skala sangat hebat",
          image: "assets/gempa_bumi.jpg"));
      list.add(ListNews(
          title: "Sumatra  Telah Diguncang Gempa Bumi  11 Skala sangat hebat",
          image: "assets/gempa_bumi.jpg"));
    });
  }

  @override
  void initState() {
    addListNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 150,
                width: double.infinity,
                color: Colors.blue,
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "BREAKING NEWS",
                      style: TextStyle(color: Colors.white),
                      textScaleFactor: 1.6,
                    ),
                    Text(
                      "Berita Bencana Gempa Bumi",
                      style: TextStyle(color: Colors.white),
                      textScaleFactor: 1.3,
                    ),
                  ],
                ),
              )),
          Positioned(
              top: 152,
              left: 15,
              right: 15,
              bottom: 10,
              child: ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (c, i) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Card(
                        elevation: 4,
                        child: Container(
                          child: Stack(
                            children: [
                              Image.asset(list[i].image,fit: BoxFit.cover,width: double.infinity,),
                              Positioned(
                                  bottom: 10,
                                  left: 10,
                                  right: 10,
                                  child: Text(list[i].title,
                                    textScaleFactor: 1.4,
                                    style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))
                            ],
                          ),
                        ),
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}

class ListNews {
  final String title;
  final String image;

  ListNews({this.title, this.image});
}
