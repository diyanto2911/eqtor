import 'dart:ui';

import 'package:eqtor/utils/screen_config.dart';
import 'package:eqtor/views/v_index.dart';
import 'package:flutter/material.dart';

class ViewLogin extends StatefulWidget {
  @override
  _ViewLoginState createState() => _ViewLoginState();
}

class _ViewLoginState extends State<ViewLogin> {
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
              top: 0,
              left: -10,
              right: -10,
              child: Image.asset(
                "assets/header.png",
                fit: BoxFit.cover,
              )),
          Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Image.asset(
                "assets/logo.png",
                height: ScreenConfig.blockHorizontal * 60,
              )),
          Positioned(
            top: ScreenConfig.blockVertical * 30,
            left: 50,
            right: 50,
            bottom: 10,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: [
                TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: Colors.blue)),
                      hintText: "Your Email",
                      hintStyle: TextStyle(color: Colors.blue)),
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: Colors.blue)),
                      hintText: "Your Password",
                      hintStyle: TextStyle(color: Colors.blue)),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: RaisedButton(
                    color: Colors.blue,
                    padding: const EdgeInsets.symmetric(horizontal: 60,vertical: 13),
                    onPressed: ()=> Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>ViewIndex())),
                    child: Text("Login",textScaleFactor: 1.2,style: TextStyle(fontSize: 18,color: Colors.white,letterSpacing: 1),),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                  ),
                ),
                SizedBox(
                  height: 120,
                ),
               RaisedButton(
                    color: Colors.blue,
                    padding: const EdgeInsets.symmetric(horizontal: 60,vertical: 13),
                    onPressed: () {},
                    child: Text("Sign Up",textScaleFactor: 1.2,style: TextStyle(fontSize: 18,color: Colors.white,letterSpacing: 1),),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                SizedBox(
                  height: 10,
                ),
                RaisedButton(
                  color: Colors.blue,
                  padding: const EdgeInsets.symmetric(horizontal: 60,vertical: 13),
                  onPressed: () {},
                  child: Text("Forget Password?",textScaleFactor: 1.2,style: TextStyle(fontSize: 18,color: Colors.white,letterSpacing: 1),),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                ),

              ],
            ),
          ),
        ],
      ),
    );
  }
}
