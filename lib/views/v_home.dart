import 'package:eqtor/utils/screen_config.dart';
import 'package:eqtor/views/v_maps.dart';
import 'package:flutter/material.dart';

class ViewHome extends StatefulWidget {
  @override
  _ViewHomeState createState() => _ViewHomeState();
}

class _ViewHomeState extends State<ViewHome> {
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset("assets/header1.png",fit: BoxFit.fitWidth,width: ScreenConfig.screenWidth,),
                Align(alignment: Alignment.topCenter,
                  child: Image.asset(
                    "assets/logo.png",
                    height: ScreenConfig.blockHorizontal * 60,
                  ),),

              ],
            ),
            SizedBox(height: 80,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewMaps())),
                  child: Container(

                    width: ScreenConfig.blockHorizontal*40,
                    height: ScreenConfig.blockHorizontal*40,
                    padding: EdgeInsets.symmetric(horizontal: 10,vertical: 16),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blue,width: 5),
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child:  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.place,size: 39,),
                        Text("Maps",textScaleFactor: 1.4,)
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: (){},
                  child: Container(

                    width: ScreenConfig.blockHorizontal*40,
                    height: ScreenConfig.blockHorizontal*40,
                    padding: EdgeInsets.symmetric(horizontal: 10,vertical: 16),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blue,width: 5),
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child:  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.settings_remote,size: 39,),
                        Text("Automatic System",textAlign: TextAlign.center,textScaleFactor: 1.4,)
                      ],
                    ),
                  ),
                ),
              ],
            ),



          ],
        ),
      ),

    );
  }
}
