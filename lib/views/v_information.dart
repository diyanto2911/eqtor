import 'package:eqtor/utils/screen_config.dart';
import 'package:flutter/material.dart';

class ViewInformation extends StatefulWidget {
  @override
  _ViewInformationState createState() => _ViewInformationState();
}

class _ViewInformationState extends State<ViewInformation> {
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset("assets/header1.png",fit: BoxFit.fitWidth,width: ScreenConfig.screenWidth,),
                Align(alignment: Alignment.topCenter,
                child: Image.asset(
                  "assets/logo.png",
                  height: ScreenConfig.blockHorizontal * 60,
                ),),

              ],
            ),
            SizedBox(height: 80,),
            InkWell(
              onTap: (){},
              child: Container(

                width: ScreenConfig.blockHorizontal*80,
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 16),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(10)
                ),
                child:  Text("Tips Perlindungan Diri",textAlign: TextAlign.center,textScaleFactor: 1.2,style: TextStyle(fontSize: 18,color: Colors.white,letterSpacing: 1),),
              ),
            ),
            
            SizedBox(height: 20,),
            InkWell(
              onTap: (){},
              child: Container(

                width: ScreenConfig.blockHorizontal*80,
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 16),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(10)
                ),
                child:  Text("Artikel Informasi",textAlign: TextAlign.center,textScaleFactor: 1.2,style: TextStyle(fontSize: 18,color: Colors.white,letterSpacing: 1),),
              ),
            ),
            SizedBox(height: 20,),
            InkWell(
              onTap: (){},
              child: Container(

                width: ScreenConfig.blockHorizontal*80,
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 16),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(10)
                ),
                child:  Text("Info BMKG",textAlign: TextAlign.center,textScaleFactor: 1.2,style: TextStyle(fontSize: 18,color: Colors.white,letterSpacing: 1),),
              ),
            ),

          ],
        ),
      ),

    );
  }
}
