import 'package:bezier_chart/bezier_chart.dart';

import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

class ViewGrahp extends StatefulWidget {
  @override
  _ViewGrahpState createState() => _ViewGrahpState();
}

class _ViewGrahpState extends State<ViewGrahp> {

  Map<String, double> dataMap = {
    "Flutter": 5,
    "React": 3,
    "Xamarin": 2,
    "Ionic": 2,
  };

  List<Color> colorList = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.yellow,
  ];

  ChartType _chartType = ChartType.disc;
  bool _showCenterText = true;
  double _ringStrokeWidth = 32;
  double _chartLegendSpacing = 32;

  bool _showLegendsInRow = false;
  bool _showLegends = true;

  bool _showChartValueBackground = true;
  bool _showChartValues = true;
  bool _showChartValuesInPercentage = false;
  bool _showChartValuesOutside = false;

//  LegendShape _legendShape = LegendShape.Circle;
  LegendPosition _legendPosition = LegendPosition.right;

  int key = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Earhquake's Values : ",style: TextStyle(color: Colors.green),textScaleFactor: 1.6,),
                Padding(
                  padding: const EdgeInsets.only(right:20.0),
                  child: Text("6.5",style: TextStyle(color: Colors.green),textScaleFactor: 1.6,),
                ),
              ],
              ),
              chartLine(context),
              SizedBox(height: 40,),
              pieChart(context)

            ],
          ),
        ),
      ),
    );
  }


  Widget chartLine(BuildContext context) {
    return Center(
      child: Container(
//        color: Colors.white,
        height: MediaQuery.of(context).size.height / 3.4,
        width: MediaQuery.of(context).size.width * 0.9,
        child: BezierChart(
          bezierChartScale: BezierChartScale.CUSTOM,
          xAxisCustomValues: const [0, 5, 10, 15, 20, 25, 30, 35],
          series: const [
            BezierLine(
              lineColor: Colors.blue,
              data: const [
                DataPoint<double>(value: 10, xAxis: 0),
                DataPoint<double>(value: 130, xAxis: 5),
                DataPoint<double>(value: 50, xAxis: 10),
                DataPoint<double>(value: 150, xAxis: 15),
                DataPoint<double>(value: 75, xAxis: 20),
                DataPoint<double>(value: 0, xAxis: 25),
                DataPoint<double>(value: 5, xAxis: 30),
                DataPoint<double>(value: 45, xAxis: 35),
              ],
            ),
          ],
          config: BezierChartConfig(
            verticalIndicatorStrokeWidth: 3.0,
            verticalIndicatorColor: Colors.black26,
            xLinesColor: Colors.blue,
            bubbleIndicatorColor: Colors.blue,
            showVerticalIndicator: true,
//            backgroundColor: Colors.white,
            snap: false,
            bubbleIndicatorLabelStyle: TextStyle(color: Colors.blue),
            bubbleIndicatorTitleStyle: TextStyle(color: Colors.blue),
            xAxisTextStyle: TextStyle(color: Colors.blue),
            yAxisTextStyle: TextStyle(color: Colors.blue),
          ),
        ),
      ),
    );
  }

  Widget pieChart(BuildContext context){
    return PieChart(
      dataMap: dataMap,
      animationDuration: Duration(milliseconds: 800),
      chartLegendSpacing: 32.0,
      chartRadius: MediaQuery.of(context).size.width / 2.7,
      showChartValuesInPercentage: true,
      showChartValues: true,
      showChartValuesOutside: false,
      chartValueBackgroundColor: Colors.grey[200],
      colorList: colorList,
      showLegends: true,
      legendPosition: LegendPosition.right,
      decimalPlaces: 1,
      showChartValueLabel: true,
      initialAngle: 0,
      chartValueStyle: defaultChartValueStyle.copyWith(
        color: Colors.blueGrey[900].withOpacity(0.9),
      ),
      chartType: ChartType.disc,
    );
  }
}
