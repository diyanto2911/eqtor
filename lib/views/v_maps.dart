import 'dart:async';

import 'package:eqtor/utils/screen_config.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class ViewMaps extends StatefulWidget {
  @override
  _ViewMapsState createState() => _ViewMapsState();
}

class _ViewMapsState extends State<ViewMaps> {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  final Set<Marker> _markers = {};
  var location = new Location();
  LatLng _lastMapPosition;

  LatLng latLngDevice = new LatLng(0, 0);




  Widget maps_kantor() {
    return Container(
      height: 400,
      width: 400,
//                height: ScreenConfig.blockVertical<=7.6 ? (ScreenConfig.blockVertical*20) : (ScreenConfig.blockVertical*100),

      child: GoogleMap(
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: latLngDevice,
          zoom: 10.0,
        ),
        mapType: MapType.hybrid,
        markers: _markers,
        onCameraMove: _onCameraMove,
        myLocationEnabled: true,
        mapToolbarEnabled: true,
        onTap: (v) {
          setLokasi(v);
          setState(() {

          });
        },
        zoomGesturesEnabled: true,
        scrollGesturesEnabled: true,
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _controller.complete(controller);
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void setLokasi(LatLng latLng) {
    _markers.clear();
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(latLng.toString()),
          position: latLng,
          icon: BitmapDescriptor.defaultMarker));
//        mapController.animateCamera(CameraUpdate.newCameraPosition(
//            CameraPosition(target: _center, zoom: 19.2)));
    });
  }

  void getLocation() async {
    if (this.mounted) {
      location.onLocationChanged().listen((LocationData currentLocation) {
        try {

          print(currentLocation.latitude);
          setState(() {
            latLngDevice =
            new LatLng(currentLocation.latitude, currentLocation.longitude);

            mapController.animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(target: latLngDevice, zoom: 10.2)));
          });
          _lastMapPosition =
          new LatLng(currentLocation.latitude, currentLocation.longitude);
        } catch (e) {}
      });
    }

    print(latLngDevice);
  }



  @override
  void initState() {

    getLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return SafeArea(
      child: Scaffold(
//        drawer: Drawer(
//          elevation: 5,
//          child: ListView(
//            children: <Widget>[],
//          ),
//
        body: SizedBox.expand(
          child: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 20,
                child: maps_kantor(),
              ),


            ],
          ),
        ),

      ),
    );
  }
}


